# Add executable called "helloDemo" that is built from the source files
# "demo.cxx" and "demo_b.cxx". The extensions are automatically found.

add_executable (Game main.cpp)

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)
target_include_directories (Game PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)

# Link the executable to the Hello library. Since the Hello library has
# public include directories we will use those link directories when building

#Game
target_link_libraries (Game LINK_PUBLIC strings)
target_link_libraries (Game LINK_PUBLIC maths)
target_link_libraries (Game LINK_PUBLIC serialisation)
target_link_libraries (Game LINK_PUBLIC engine)


## For the tutorial!
IF (WIN32)
	add_executable (Client WIN32 client.cpp)
ELSE()
	add_executable (Client client.cpp)
ENDIF()

#Includes only for this target - these could possibly be for all targets but...
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)
target_include_directories (Client PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../engine)

# Link the executable to the Hello library. Since the Hello library has
# public include directories we will use those link directories when building

#Game
target_link_libraries (Client LINK_PUBLIC strings)
target_link_libraries (Client LINK_PUBLIC maths)
target_link_libraries (Client LINK_PUBLIC serialisation)
target_link_libraries (Client LINK_PUBLIC engine)

#-------------------
# INSTALL TARGET
#-------------------

message(STATUS "Where is everything going %s" ${CMAKE_CURRENT_BINARY_DIR})
message(STATUS "Where is everything going %s" ${PROJECT_BINARY_DIR})


# post-build copy for win32
if(WIN32 AND NOT MINGW)
	add_custom_command(TARGET Client PRE_BUILD
		COMMAND if not exist .\\dist mkdir .\\dist)
	add_custom_command(TARGET Client POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist )
	add_custom_command(TARGET Game PRE_BUILD
		COMMAND if not exist .\\dist mkdir .\\dist)
	add_custom_command(TARGET Game POST_BUILD
		COMMAND copy \"$(TargetPath)\" .\\dist )
endif(WIN32 AND NOT MINGW)

if(MINGW OR UNIX)
	set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist)
endif(MINGW OR UNIX)

if(MINGW OR UNIX)
	set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/dist)
endif(MINGW OR UNIX)

# Copy the Executables to the runtime directory. 

install(TARGETS Game
	CONFIGURATIONS Release RelWithDebInfo Debug
	RUNTIME DESTINATION bin
	)

install(TARGETS Client
	CONFIGURATIONS Release RelWithDebInfo Debug
	RUNTIME DESTINATION bin
	)

if(WIN32)
	# https://stackoverflow.com/questions/23950887/does-cmake-offer-a-method-to-set-the-working-directory-for-a-given-build-system
	# Ignored due to a VS issue https://developercommunity.visualstudio.com/content/problem/268817/debugger-no-longer-respects-localdebuggerworkingdi.html
	# Fix is due in the next version https://gitlab.kitware.com/cmake/cmake/-/merge_requests/6783
	set_target_properties(Game PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}/bin")
	set_target_properties(Client PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}/bin")


endif()